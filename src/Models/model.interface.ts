import {Document} from 'mongoose'


export class Movie extends Document{
    id: String
    title: String
    director: String
    cast:{
        actor: String
        actress: String
    }
}