import {Schema, Prop, SchemaFactory} from '@nestjs/mongoose'
import {Document} from 'mongoose'


export type MovieModel = Movie & Document

@Schema({timestamps: true, versionKey: false})
export class Movie{

    @Prop({required: true})
    title: String

    @Prop({required: true})
    director: String

    @Prop({required: true})
    actor: String

    @Prop({required: true})
    actress: String
}

export const movieSchema = SchemaFactory.createForClass(Movie)

