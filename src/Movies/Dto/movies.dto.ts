
import { MovieModel} from '../../Models/model'
import { Body, Injectable } from '@nestjs/common'
import {InjectModel} from '@nestjs/mongoose'
import { Model } from 'mongoose' 
import { CreateMovieDto } from 'src/Utils/input.validation'

@Injectable()
export class MovieDao{

    constructor(@InjectModel('Movies') private modelObj: Model<MovieModel>){

    }

    async getAll(){
       return await this.modelObj.find()
    }

    async createMovie(@Body() body: CreateMovieDto){
        const modelObj = new this.modelObj(body)
        return await modelObj.save()      
    }

    async deleteById(id: String){
        return await this.modelObj.findOneAndDelete({_id: id})
    }

    async getById(id: String){
        return await this.modelObj.findById({_id: id})
    }

    async updateMovie(body: CreateMovieDto, id: String){
        return await this.modelObj.findOneAndUpdate({_id: id}, body, {new: true})
    }
}