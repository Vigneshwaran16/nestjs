import { Body, Controller, Delete, Get, Param, Post, Put, Req, Res } from '@nestjs/common';
import { CreateMovieDto } from 'src/Utils/input.validation';
import { Response } from 'src/Utils/response';
import { MoviesService } from './movies.service';

@Controller('moviedb')
export class MoviesController {


    constructor(private movieService: MoviesService){

    }

    @Get('movies')
    async getAll(@Res() response){
        const result: Response= await this.movieService.getAll()
        response.status(result.statusCode).json({movies: result.message})
    }

    @Post('movie')
    async createMovie(@Res() response , @Body() body: CreateMovieDto){
        const result: Response = await this.movieService.createMovie(body)
        response.status(200).json({added: result})
    }

    @Get('movie/:id')
    async getById(@Param() params, @Res() response){
        const result: Response = await this.movieService.getById(params.id)
        response.status(result.statusCode).json(result.message)
    }

    @Put('movie/:id')
    async updateMovie(@Res() response, @Body() body, @Param() params){
        const result: Response = await this.movieService.updateMovie(body, params.id)
        response.status(result.statusCode).json({updated: result.message})
    }
    @Delete('movie/:id')
    async deleteById(@Param() params, @Res() response){
        const result: Response = await this.movieService.deleteById(params.id)
        response.status(result.statusCode).json({deleted: result.message})
    }
}
