import { Module } from '@nestjs/common'
import {MongooseModule} from '@nestjs/mongoose'
import {movieSchema} from '../Models/model'
//import {Movie} from '../Models/model.interface'
import { MovieDao } from './Dto/movies.dto';
import { MoviesController } from './movies.controller';
import { MoviesService } from './movies.service';

@Module({
    imports:[MongooseModule.forFeature([{
        name: 'Movies',
        schema: movieSchema,
        collection: 'movies'
    }]),],
    controllers:[MoviesController],
    providers: [MoviesService, MovieDao]
})
export class MoviesModule {
    
}
