import { Body, Injectable, Req } from '@nestjs/common';
import { MovieModel } from 'src/Models/model';
import { CreateMovieDto } from 'src/Utils/input.validation';
//import { Movie } from 'src/Models/model.interface';
import {MovieDao} from './Dto/movies.dto'
import { CannotDeleteMovie, CannotUpdateMovie, EmptyResponse, MovieNotFound } from '../Utils/error.handler'

@Injectable()
export class MoviesService {

    constructor(private movieDao: MovieDao){

    }

    async getAll(){
        var result = await this.movieDao.getAll()
        if(!result){
            throw new EmptyResponse('Cannot get all movies')
        }
        else{
            return {
                statusCode: 200,
                message: result
            }
        }
    }

    async createMovie(@Body() body: CreateMovieDto ){
        var result = await this.movieDao.createMovie(body)
        if(!result){
            throw new EmptyResponse('Cannot create movie')
        }
        else{
            return{
                statusCode: 200,
                message: result
            }
        }
    }

    async getById(id: String){
        var result = await this.movieDao.getById(id)
        if(!result){
            throw new MovieNotFound(id)
        }
        else{
            return {
                statusCode: 200,
                message: result
            }
        }
        
    }

    async deleteById(id: String){
        var result = await this.movieDao.deleteById(id)
        console.log('Deleted:',result)
        if(!result){
            throw new CannotDeleteMovie(id)
        }
        else{
            return{
                statusCode: 200,
                message: result
            }
        }
    }

    async updateMovie(@Body() body, id: String){
        var result = await this.movieDao.updateMovie(body,id)
        if(!result){
            throw new CannotUpdateMovie(id)
        }
        else{
            return{
                statusCode: 200,
                message: result
            }
        }
    }
}
