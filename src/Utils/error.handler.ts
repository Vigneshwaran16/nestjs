import { HttpException } from "@nestjs/common";



export class MovieNotFound extends HttpException{

    constructor(movieID: String){
        super(`Movie with ID ${movieID} not found`,404)
    }
}

export class CannotDeleteMovie extends HttpException{
    constructor(movieID: String){
        super(`Cannot delete movie with ID ${movieID}`,500)
    }
}

export class EmptyResponse extends HttpException{
    constructor(errorMessage: String){
        super(errorMessage, 500)
    }
}

export class CannotUpdateMovie extends HttpException{
    constructor(movieID: String){
        super(`Cannot update movie with ID ${movieID}`, 500)
    }

}