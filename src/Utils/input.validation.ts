import { IsString, IsNotEmpty } from 'class-validator'


export class CreateMovieDto{
    @IsNotEmpty()
    @IsString()
    title: String

    @IsNotEmpty()
    @IsString()
    director: String

    @IsNotEmpty()
    @IsString()
    actor: String

    @IsNotEmpty()
    @IsString()
    actress: String
}
