import { IsString, IsNumber, IsNotEmpty } from 'class-validator'
import { Document } from 'mongoose'
import { MovieModel } from 'src/Models/model'

export class Response{

    @IsNumber()
    statusCode: number

    @IsNotEmpty()
    message: String | MovieModel | MovieModel[] 
}