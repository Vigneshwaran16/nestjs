import { Module } from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose'
import {movieSchema} from './Models/model'
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MoviesModule } from './Movies/movies.module';

@Module({
  imports: [MoviesModule, 
    MongooseModule.forRoot('mongodb://localhost:27017/MoviesDb')],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
